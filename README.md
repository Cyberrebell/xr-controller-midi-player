# XRController Midi Player ![icon](addons/xr-controller-midi-player/icon.png)
A Godot Engine node which can play .mid files on the vibration motor of XR controllers. 

## Run the demo
* Open the project with Godot 4.0
* Run the project. Use the right hand trigger to play and left hand trigger to stop.
* Replace the music.mid with any .mid file you like.

## How to use the plugin
See [plugin documentation](addons/xr-controller-midi-player/README.md).

## License
MIT

[<img src="icon.svg">](https://godotengine.org/asset-library/asset/1536)
